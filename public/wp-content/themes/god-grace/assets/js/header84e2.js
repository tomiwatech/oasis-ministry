(function ($) {    
    $(document).ready(function() {  

        "use strict"; 

    /* Do not delete above code */

        /* Main Navigation ---------- */

        function godgrace_main_navigation() {

            var godgrace_GetMegaMenuWidth = $('#godgrace_header').width(),
                godgrace_FinalWidth = (godgrace_GetMegaMenuWidth * 2) / 100,
                godgrace_m_finalWidth = Math.round(godgrace_GetMegaMenuWidth - godgrace_FinalWidth);

            $('.godgrace_fw_header ul.godgrace_top_nav li.godgrace-mega-menu > ul').css({"max-width": godgrace_m_finalWidth + 'px', "width": godgrace_m_finalWidth + 'px'});

            /* header title top margin */

            var godgrace_GetHeaderTitleHeight = $('.godgrace_pg_title h1').height() / 2;
            $('.godgrace_pg_title').css('margin-top','-' + godgrace_GetHeaderTitleHeight + 'px');

            var godgrace_GetHeaderLargeTitleHeight = $('.godgrace_large_title span').height() / 2;
            $('.godgrace_large_title').css('margin-top','-' + godgrace_GetHeaderLargeTitleHeight + 'px');    

            /* create mobile menu */

            $(".godgrace_main_nav > ul").clone().appendTo("#godgrace_mobi_nav");
            $('#godgrace_mobi_nav > ul').removeClass('godgrace_top_nav');
            $('#godgrace_mobi_nav').find('.godgrace_nav_p_meta').remove();

            /* mobile dropdown menu */

            function godgrace_mobile_menu(godgrace_main_nav,click_button,nav_id) {
              
                var godgrace_main_nav = $(godgrace_main_nav);

                $(click_button).on('click', function(){
                    var godgrace_dd_menu = $(nav_id);
                    if (godgrace_dd_menu.hasClass('open')) { 
                        godgrace_dd_menu.slideUp(400).removeClass('open');
                    }
                    else {
                        godgrace_dd_menu.slideDown(400).addClass('open');
                    }
                });

                godgrace_main_nav.find('li ul').parent().addClass('godgrace-has-sub-menu');          
                godgrace_main_nav.find(".godgrace-has-sub-menu").prepend('<span class="godgrace-mini-menu-arrow"><i class="fa fa-angle-down"></i></span>');

                godgrace_main_nav.find('.godgrace-mini-menu-arrow').on('click', function() {
                    if ($(this).siblings('ul').hasClass('open')) {
                        $(this).siblings('ul').removeClass('open').hide('blind');
                    }
                    else {
                        $(this).siblings('ul').addClass('open').show('blind');
                    }
                });       

            }

            godgrace_mobile_menu('#godgrace_mobi_nav','#godgrace_mobi_nav_btn','#godgrace_mobi_nav > ul');
            
        }

        /* mega menu */

        function godgrace_GetCenter() {

            var godgrace_GetMegaMenuWidth =  $('#godgrace_header .godgrace_container .godgrace_nav').width(),
                godgrace_GetMainNavUlWidth = $('.godgrace_main_nav').width(),
                godgrace_CenterDiv = $('ul.godgrace_top_nav > li.godgrace-mega-menu > ul'),
                godgrace_GetLeftMargin = (godgrace_GetMegaMenuWidth - godgrace_GetMainNavUlWidth) * (-1);

                $(godgrace_CenterDiv).css('left',godgrace_GetLeftMargin );

                $('ul.godgrace_top_nav li.godgrace-mega-menu > ul').css({"max-width": godgrace_GetMegaMenuWidth + 'px', "width": godgrace_GetMegaMenuWidth + 'px' });

        }      

        // logo section toggle responsive menu

        function godgrace_logo_section_dropdown() {

            $('.godgrace_contactbar_content_mobile_btn').click(function () {
                $('.godgrace_contactbar_content_mobile_wrap').toggleClass("logo_nav_active");        

                var menu_icon = $('.godgrace_contactbar_content_mobile_btn span i');

                if($(menu_icon).hasClass("fa-minus")) {        
                    $(menu_icon).removeClass("fa-minus").addClass("fa-plus");
                } else {
                    $(menu_icon).removeClass("fa-plus").addClass("fa-minus");        
                } 
            });
        }



        /* Load All Functions ---------- */

        $(document).ready(function() { 
            godgrace_main_navigation();
            godgrace_GetCenter();      
            godgrace_logo_section_dropdown();      

            
         });

       

    /* Do not delete below code */

    }); 
})(jQuery);


